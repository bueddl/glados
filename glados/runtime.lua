require "debug"

Runtime = {}

function Runtime.pureVirtualCall()
  Debug.traceback()
  error("Pure virtual function call")
end
