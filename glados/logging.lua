require "debug"
require "runtime"
require "event"
require "ui"

--

Logging = {}

Logging.LogLevel = {
  trace = 0,
  debug = 1,
  info = 2,
  warn = 3,
  error = 4,
  critical = 5,
}

function Logging.LogLevel.ToString(log_level)
  local names = {
    [Logging.LogLevel.trace] = "trace",
    [Logging.LogLevel.debug] = "debug",
    [Logging.LogLevel.info] = "info",
    [Logging.LogLevel.warn] = "warn",
    [Logging.LogLevel.error] = "error",
    [Logging.LogLevel.critical] = "critical"
  }

  if names[log_level] == nil then
    return "-"
  end

  return names[log_level]
end

--

Logging.Logger = {}

function Logging.Logger:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.backlog_size = o.backlog_size or 1000
  o.backlog = {}
  return o  
end

function Logging.Logger:log(severity, subsystem, fmt, ...)
  local log_entry = {
    severity = severity,
    subsystem = subsystem,
    format = fmt,
    args = {...},
    day = os.day(),
    time = os.time(),
  }
  table.insert(self.backlog, log_entry)

  while table.getn(self.backlog) > self.backlog_size do
    table.remove(self.backlog, 1)
  end

  self.event_loop:queueEvent("new_log_entry", log_entry)
end

function Logging.Logger:trace(...)
  self:log(Logging.LogLevel.trace, ...)
end

function Logging.Logger:debug(...)
  self:log(Logging.LogLevel.debug, ...)
end

function Logging.Logger:info(...)
  --[[
  local calling_func = debug.getinfo(1)
  local _, calling_object = debug.getlocal(1, 1)
  local object_mt = debug.getmetatable(calling_object)
  Debug.dump(calling_func)
  Debug.dump(calling_object)
  Debug.dump(object_mt)
  ]]
  self:log(Logging.LogLevel.info, ...)
end

function Logging.Logger:warn(...)
  self:log(Logging.LogLevel.warn, ...)
end

function Logging.Logger:error(...)
  self:log(Logging.LogLevel.error, ...)
end

function Logging.Logger:critical(...)
  self:log(Logging.LogLevel.critical, ...)
end

function Logging.Logger:getLastEntries(n)
  local to_index = table.getn(self.backlog)
  local from_index = to_index - n
  if from_index < 1 then
    from_index = 1
  end

  return {unpack(self.backlog, from_index, to_index)}
end

function Logging.Logger:getBacklogMessageCount(severity, accept_higher_levels)
  if severity == nil then
    return #self.backlog
  end
  if accept_higher_levels == nil then
    accept_higher_levels = false
  end

  local count = 0
  for i, entry in pairs(self.backlog) do
    if entry.severity == severity or accept_higher_levels and entry.severity >= severity then
      count = count + 1
    end
  end
  return count
end

--

Logging.LogLinesWidget = UI.Widget:new()

function Logging.LogLinesWidget:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  self.loglines = {}
  return o
end

function Logging.LogLinesWidget:doLayout()
end

function Logging.LogLinesWidget:getPreferredWidth()
  return 402
end

function Logging.LogLinesWidget:getPreferredHeight()
  return 300
end

function Logging.LogLinesWidget:onMouseButtonPressed(mouse_button, x, y)
end

function Logging.LogLinesWidget:repaint()
  self.h.setBackgroundColor(colors.black)

  for i, log_entry in ipairs(self.loglines) do
    local formatted_entry = self:formatEntry(log_entry)

    self.h.setCursorPos(1, i)
    local remaining_length = self.width - string.len(formatted_entry.text)
    if remaining_length < 0 then
      remaining_length = 0
    end
    self.h.blit(
      formatted_entry.text .. string.rep(" ", remaining_length),
      formatted_entry.foreground_colors .. string.rep("f", remaining_length), 
      formatted_entry.background_colors .. string.rep("f", remaining_length))
  end
end

function Logging.LogLinesWidget:formatEntry(log_entry)
  local time_str = textutils.formatTime(log_entry.time, true)
  if string.len(time_str) == 4 then
    time_str = "0" .. time_str
  end

  local name_for_severity = Logging.LogLevel.ToString(log_entry.severity)

  local log_fmt = "[%03d,%s][%s][%s] " .. log_entry.format
  local message_text = string.format(log_fmt, 
    log_entry.day,
    time_str,
    name_for_severity,
    log_entry.subsystem, 
    unpack(log_entry.args))

  local color_map = {
    [Logging.LogLevel.trace] = "8",
    [Logging.LogLevel.debug] = "0",
    [Logging.LogLevel.info] = "3",
    [Logging.LogLevel.warn] = "4",
    [Logging.LogLevel.error] = "e",
    [Logging.LogLevel.critical] = "a"
  }

  local color_for_severity = color_map[log_entry.severity]
  local foreground_colors = string.rep("7", 1) 
    .. string.rep("8", 9) 
    .. string.rep("7", 2) 
    .. string.rep(color_for_severity, string.len(name_for_severity))
    .. string.rep("7", 2) 
    .. string.rep("8", string.len(log_entry.subsystem))
    .. string.rep("7", 1) 
    .. string.rep("0", string.len(message_text) - string.len(name_for_severity) - string.len(log_entry.subsystem) - 15)
  local background_colors = string.rep("f", string.len(message_text))

  return {
    text = message_text, 
    foreground_colors = foreground_colors, 
    background_colors = background_colors
  }
end

function Logging.LogLinesWidget:setLogLines(loglines)
  self.loglines = loglines
end

--

Logging.LogScrollWidget = UI.ScrollBox:new()

function Logging.LogScrollWidget:new(o)
  o = UI.ScrollBox:new(o)
  setmetatable(o, self)
  self.__index = self
  o.event_loop = o.logger.event_loop
  return o
end

function Logging.LogScrollWidget:setupUI()
  self.loglines = Logging.LogLinesWidget:new()
  self:setWidget(self.loglines)
  UI.ScrollBox.setupUI(self)

  self.event_loop:registerHandler(self, {
    "new_log_entry"
  })
end

function Logging.LogScrollWidget:dispatchEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  local event_handled = self:doDispatch(
    UI.DrawSurface.dispatchEvent,
    {
      new_log_entry = "onNewLogEntry",
    },
    event_name, arg1, arg2, arg3, arg4, arg5)

  return event_handled
end

function Logging.LogScrollWidget:onNewLogEntry(_)
  local lines_to_show = self.loglines:clientHeight() -- TODO
  local backlog = self.logger:getLastEntries(lines_to_show)

  self.loglines:setLogLines(backlog)
  self:repaint()
end

--

Logging.LogViewWidget = UI.Widget:new()

function Logging.LogViewWidget:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.logger = o.logger
  o.layout = UI.VBoxLayout:new{widget = o}
  return o
end

function Logging.LogViewWidget:setupUI()
  self.log_scroll = Logging.LogScrollWidget:new{logger = self.logger}
  self.log_scroll:setupUI()
  self.layout:addWidget(self.log_scroll)

  local statusbar = UI.StatusBar:new()

  statusbar:addItem("Message Count: ")
  self.message_count_label = UI.Label:new{text="-"}
  statusbar:addItem(self.message_count_label)

  statusbar:addItem("Warnings: ")
  self.warning_count_label = UI.Label:new{text="-"}
  statusbar:addItem(self.warning_count_label)

  statusbar:addItem("Errors: ")
  self.error_count_label = UI.Label:new{text="-"}
  statusbar:addItem(self.error_count_label)

  statusbar:setupUI()
  self.layout:addWidget(statusbar)

  self.logger.event_loop:registerHandler(self, {
    "new_log_entry"
  })
end

function Logging.LogViewWidget:dispatchEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  local event_handled = self:doDispatch(
    UI.DrawSurface.dispatchEvent,
    {
      new_log_entry = "onNewLogEntry",
    },
    event_name, arg1, arg2, arg3, arg4, arg5)

  return event_handled
end

function Logging.LogViewWidget:onNewLogEntry(_)
  self.message_count_label:setText(
    tostring(self.logger:getBacklogMessageCount()))

  self.warning_count_label:setText(
    tostring(self.logger:getBacklogMessageCount(Logging.LogLevel.warn)))

  self.error_count_label:setText(
    tostring(self.logger:getBacklogMessageCount(Logging.LogLevel.error, true)))

  self:repaint()
end

--


