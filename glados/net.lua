require "debug"
require "runtime"
require "event"

--

Networking = {}

-- 
-- different types of modems
-- =========================
-- 
-- directly attached: attached peripherals (modem, wireless_modem), can be used via modem API.
-- 
-- reachable devices: attached to another computer that is reachable from the current computer.
-- 

Networking.ModemManager = {}

function Networking.ModemManager:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  self.modems = {}
  o:openAll()
  return o
end

function Networking.ModemManager:openAll()
  local peripheral_list = peripheral.getNames()

  for i = 1, #peripheral_list do
    local this_peripheral_side = peripheral_list[i]
    --print("peripheral", this_peripheral_side, peripheral.getType(this_peripheral_side))
    if peripheral.isPresent(this_peripheral_side) and peripheral.getType(this_peripheral_side) == "modem" then
      --modem.open(os.getComputerID())
    end
  end
end

--

Networking.ErrorCode = {}

function Networking.ErrorCode:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  self.error_code = 0
  return o
end

function Networking.ErrorCode:isSuccess()
  return self.error_code == 0
end

--

Networking.Buffer = {}

function Networking.Buffer:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--

Networking.Acceptor = {}

function Networking.Acceptor:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Networking.Acceptor:listen(port)
end

function Networking.Acceptor:asyncAccept(socket, accept_handler)
  require "system"

  SystemManagement.WorkContext:post(function()
    self:asyncAccept(socket, accept_handler)
    --accept_handler(Networking.ErrorCode:new())
  end)
end

--

Networking.Socket = {}

function Networking.Socket:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Networking.Socket:asyncReceive(buffer, completion_handler)
  require "system"
  
  SystemManagement.WorkContext:post(function()
    completion_handler(Networking.ErrorCode:new(), 0)
  end)
end
