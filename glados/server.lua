require "net"
require "system"

--

Server = {}

--

Server.Server = {}

function Server.Server:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o  
end

function Server.Server:start(port)
  self.acceptor = Networking.Acceptor:new{executor=SystemManagement.WorkContext}
  self.acceptor:listen(port)
  self:startAccept()
end

function Server.Server:startAccept()
  SystemManagement.Logger:info("Server.Server", "startAccept")

  local socket = Networking.Socket:new{executor=SystemManagement.WorkContext}
  self.acceptor:asyncAccept(socket, function(ec)
    self:startAccept()

    if not ec:isSuccess() then
      return
    end

    local client = Server.ClientHandler:new{socket=socket}
    client:startReceive()
  end)
end

--

Server.ClientHandler = {}

function Server.ClientHandler:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o  
end

function Server.ClientHandler:startReceive()
  local buffer = Networking.Buffer:new()

  self.socket:asyncReceive(buffer, function (ec, bytes_received)
    self:startReceive()

    -- TODO
  end)
end
