require "ui"
require "debug"

--

UI.Loader = {}

--

UI.Loader.Loader = {}

function UI.Loader.Loader:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function UI.Loader.Loader:load(settings)
  self:loadUI(settings)
end

function UI.Loader.Loader:loadUI(settings)
  require "system"

  local ui_settings = settings.get("ui")
  local peripheral_list = peripheral.getNames()

  for i = 1, #peripheral_list do
    local this_peripheral_side = peripheral_list[i]
    local peripheral_type = peripheral.getType(this_peripheral_side)

    if peripheral_type == "monitor" then
      local view_index = nil
      for monitor_index, monitor_side in ipairs(ui_settings.monitors) do
        if monitor_side == this_peripheral_side then
          view_index = monitor_index
        end
      end

      local monitor = SystemManagement.UI:wrapMonitor(this_peripheral_side)

      if view_index == nil then
        require "dummy_widget"

        SystemManagement.Logger:info("UI.Loader", "Monitor without an assigned view; using default view")

        monitor:setupUI()
        local perspective = SystemManagement.UI:createPerspective("Unnamed")
        local content_widget = DummyWidget.DummyWidget:new{color = colors.black, text_color = colors.white}
        content_widget:setupUI()
        perspective:setWidget(content_widget)
        monitor:addTab("default", "Unnamed", perspective)
        monitor:doLayout()
      else
        local view = ui_settings.views[view_index]
        self:loadView(view.perspectives, monitor)

        if view.active_perspective ~= nil then
          monitor:setActiveTab(view.active_perspective)
        end
      end
    end
  end
end

function UI.Loader.Loader:loadView(perspectives, monitor)
  monitor:setupUI()
  local late_task_list = {}

  for key, perspective_info in pairs(perspectives) do
    perspective = self:loadPerspective(perspective_info, late_task_list)
    monitor:addTab(key, perspective_info.name, perspective)
  end
  
  monitor:doLayout()

  for _, task in ipairs(late_task_list) do
    task()
  end
end

function UI.Loader.Loader:loadPerspective(perspective_info, late_task_list)
  local perspective = SystemManagement.UI:createPerspective(perspective_info.name)
  self:loadSplitView(perspective, perspective_info.content, late_task_list)
  return perspective
end

function UI.Loader.Loader:loadSplitView(parent, view_info, late_task_list)
  require "dummy_widget"

  if view_info.type == "SplitView" then
    local count_splits = # view_info.splits
    
    local splits
    if view_info.direction == "vertical" then
      splits = { parent:splitHorizontal(count_splits) }
    elseif view_info.direction == "horizontal" then
      splits = { parent:splitVertical(count_splits) }
    else
      SystemManagement.Logger.warn("UI.Loader", "Invalid direction in configuration '%s'", view_info.direction)
      return
    end

    for index, child_view_info in ipairs(view_info.splits) do
      local split = splits[index]
      self:loadSplitView(split, child_view_info, late_task_list)
    end

    for splitter_index, splitter_position in pairs(view_info.splitters) do
      table.insert(late_task_list, function()
        parent.layout:setSplitterPositionAbsoluteByIndex(splitter_index, splitter_position)
      end)
    end
  else
    local widget_tables = {
      ["DummyWidget.DummyWidget"] = DummyWidget.DummyWidget,
      ["Logging.LogViewWidget"] = Logging.LogViewWidget,
      ["DummyWidget.ExampleScrollWidget"] = DummyWidget.ExampleScrollWidget,
    }

    local widget_table = widget_tables[view_info.type]
    local content_widget = widget_table:new{
      logger=SystemManagement.Logger,
    }
    content_widget:setupUI()
    parent:setWidget(content_widget)
  end
end