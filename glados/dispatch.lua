require "debug"
require "runtime"

--

Dispatch = {}

--

Dispatch.DispatchLoop = {}

function Dispatch.DispatchLoop:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.function_queue = {}
  return o
end

function Dispatch.DispatchLoop:run()
  require "system"

  SystemManagement.Logger:info("Dispatch.DispatchLoop", "Starting Work Executor")

  while #self.function_queue > 0 do
    self:doRunOne()
    coroutine.yield()
  end
end

function Dispatch.DispatchLoop:doRunOne()
  local fn = table.remove(self.function_queue, 1)
  fn()
end

function Dispatch.DispatchLoop:post(fn)
  table.insert(self.function_queue, fn)
  --Debug.print("posted new function, #", #self.function_queue)
end