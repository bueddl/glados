require "debug"
require "runtime"
require "event"

--

UI = {}

--

UI.DrawSurface = Event.EventHandler:new()

function UI.DrawSurface:new(o)
  o = Event.EventHandler:new(o)
  setmetatable(o, self)
  self.__index = self
  if o.h then
    o.width, o.height = o.h.getSize()
  end
  o.needs_repaint = true
  return o
end

function UI.DrawSurface:dispatchEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  return self:doDispatch(
    Event.EventHandler.dispatchEvent,
    {
      char = "onCharTyped",
      key = "onKeyPressed",
      paste = "onClipboardContentsPasted",
      mouse_click = "onMouseButtonPressed",
      mouse_scroll = "onMouseScroll",
      mouse_drag = "onMouseDrag",
    },
    event_name, arg1, arg2, arg3, arg4, arg5)
end

function UI.DrawSurface:invalidate()
  self.needs_repaint = true
end

function UI.DrawSurface:repaint()
  self.needs_repaint = false
end

--

UI.Layout = {}

function UI.Layout:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.child_widgets = {}
  return o
end

function UI.Layout:addWidget(widget)
  table.insert(self.child_widgets, widget)
end

function UI.Layout:doLayout()
  Runtime.pureVirtualCall()
end

function UI.Layout:updateChilds(child_pos_and_sizes)
  local all_have_fixed_height = true
  local maximum_fixed_height = 0
  local all_have_fixed_width = true
  local maximum_fixed_width = 0
  local has_children = #child_pos_and_sizes > 0

  for _, v in ipairs(child_pos_and_sizes) do
    v.widget.h = v.widget.h or window.create(self.widget.h, 1, 1, 1, 1)
    v.widget.h.absolute_position = {
      x = self.widget.h.absolute_position.x + v.x - 1,
      y = self.widget.h.absolute_position.y + v.y - 1}
    v.widget:setPosition(v.x, v.y)
    v.widget:setSize(v.width, v.height)
    v.widget:doLayout()

    all_have_fixed_height = all_have_fixed_height and v.widget:getLayoutHeightHint() == UI.LayoutSizeHint.Fixed
    if all_have_fixed_height and v.height > maximum_fixed_height then
      maximum_fixed_height = v.height
    end

    all_have_fixed_width = all_have_fixed_width and v.widget:getLayoutWidthHint() == UI.LayoutSizeHint.Fixed
    if all_have_fixed_width and v.width > maximum_fixed_width then
      maximum_fixed_width = v.width
    end
  end

  if has_children and all_have_fixed_height and self.widget:getLayoutHeightHint() == UI.LayoutSizeHint.Auto then
    self.widget:setFixedHeightOverride(maximum_fixed_height)
  end

  if has_children and all_have_fixed_width and self.widget:getLayoutWidthHint() == UI.LayoutSizeHint.Auto then
    self.widget:setFixedWidthOverride(maximum_fixed_width)
  end
end

--

UI.NullLayout = UI.Layout:new()

function UI.NullLayout:new(o)
  o = UI.Layout:new(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

function UI.NullLayout:doLayout()
end

--

UI.HBoxLayout = UI.Layout:new()

function UI.HBoxLayout:new(o)
  o = UI.Layout:new(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

--

UI.BoxLayoutDirection = {
  Horizontal = 1,
  Vertical = 2,
}

UI.BoxLayout = UI.Layout:new()

function UI.BoxLayout:new(o)
  o = UI.Layout:new(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

function UI.BoxLayout:getClientSizeInLayoutDirection()
  if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
     return self.widget:clientWidth()
  elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
     return self.widget:clientHeight()
  else
    Debug.print(self, self.layout_direction)
    Debug.traceback()
  end
end

function UI.BoxLayout:getLayoutSizeHintOf(widget)
  if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
     return widget:getLayoutWidthHint()
  elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
     return widget:getLayoutHeightHint()
  end
end

function UI.BoxLayout:getPreferredSizeOf(widget)
  if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
     return widget:getPreferredWidth()
  elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
     return widget:getPreferredHeight()
  end
end

function UI.BoxLayout:getMinimumSizeOf(widget)
  if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
     return widget:getMinimumWidth()
  elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
     return widget:getMinimumHeight()
  end
end

function UI.BoxLayout:getMaximumSizeOf(widget)
  if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
     return widget:getMaximumWidth()
  elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
     return widget:getMaximumHeight()
  end
end

function UI.BoxLayout:doLayout()
  local available_size = self:getClientSizeInLayoutDirection()
  local allocated_size = 0
  local sizes = {}
  for i, child_widget in ipairs(self.child_widgets) do
    -- getLayoutHeightHint() -> fixed | auto*
    -- if fixed then getPreferredHeight()
    -- if auto then distribute equally but respect min and max
    local size_hint = self:getLayoutSizeHintOf(child_widget)
    if size_hint == UI.LayoutSizeHint.Fixed then
      initial_size = self:getPreferredSizeOf(child_widget)
    elseif size_hint == UI.LayoutSizeHint.Auto then
      initial_size = math.floor(available_size / #self.child_widgets)
    end
    allocated_size = allocated_size + initial_size

    table.insert(sizes, initial_size)
  end

  local shrink_needed = allocated_size > available_size
  if shrink_needed then
    local shrink_amount_needed = allocated_size - available_size
    local shrink_amount_resolved = 0

    -- if total height is greater than available, then decrease each widget until its min size, if any (not for fixed size)
    while shrink_amount_needed > shrink_amount_resolved do
      local could_legitimately_shrink = false

      for i,child_widget in ipairs(self.child_widgets) do
        local proposed_size = sizes[i]

        local is_auto_size = self:getLayoutSizeHintOf(child_widget) == UI.LayoutSizeHint.Auto
        local can_shrink = proposed_size > self:getMinimumSizeOf(child_widget)
        local can_shrink_legitmately = is_auto_size and can_shrink
        local still_needs_shrink = shrink_amount_needed > shrink_amount_resolved

        if still_needs_shrink and can_shrink_legitmately then
          sizes[i] = proposed_size - 1
          shrink_amount_resolved = shrink_amount_resolved + 1
          could_legitimately_shrink = true
        end
      end

      -- if still above available height, then fuck it and decrease each and every widget as long as its height is gt 0
      if not could_legitimately_shrink then
        local did_force_shrink = false
        for i,child_widget in ipairs(self.child_widgets) do
          local proposed_size = sizes[i]
          local still_needs_shrink = shrink_amount_needed > shrink_amount_resolved
          local can_shrink = proposed_size > 0
          if still_needs_shrink and can_shrink then
            sizes[i] = proposed_size - 1
            shrink_amount_resolved = shrink_amount_resolved + 1
            did_force_shrink = true
          end
        end

        if not did_force_shrink then
          break
        end
      end
    end

    allocated_size = allocated_size - shrink_amount_resolved
  end

  local grow_needed = allocated_size < available_size
  if grow_needed then
    local grow_amount_needed = available_size - allocated_size
    local grow_amount_resolved = 0

    -- if total height is lower than available, then increase each widget until its max size, if any (not for fixed size)
    while grow_amount_needed > grow_amount_resolved do
      local could_legitimately_grow = false

      for i,child_widget in ipairs(self.child_widgets) do
        local proposed_size = sizes[i]

        local is_auto_size = self:getLayoutSizeHintOf(child_widget) == UI.LayoutSizeHint.Auto
        local can_grow = self:getMaximumSizeOf(child_widget) == -1 or proposed_size < self:getMaximumSizeOf(child_widget)
        local can_grow_legitmately = is_auto_size and can_grow
        local still_needs_grow = grow_amount_needed > grow_amount_resolved

        if still_needs_grow and can_grow_legitmately then
          sizes[i] = proposed_size + 1
          grow_amount_resolved = grow_amount_resolved + 1
          could_legitimately_grow = true
        end
      end

      -- if still below available height, then leave it like that.
      if not could_legitimately_grow then
        break
      end
    end
    
    allocated_size = allocated_size + grow_amount_resolved
  end

  local x = 1;
  local y = 1

  local child_pos_and_sizes = {}
  for i,v in ipairs(sizes) do
    local child_widget = self.child_widgets[i]

    local child_height = self.widget:clientHeight()
    if self.layout_direction == UI.BoxLayoutDirection.Vertical then 
      child_height = sizes[i]
    else
      if child_widget:getMaximumHeight() ~= -1 and child_widget:getMaximumHeight() < child_height then
        child_height = child_widget:getMaximumHeight()
      end
    end

    local child_width = self.widget:clientWidth()
    if self.layout_direction == UI.BoxLayoutDirection.Horizontal then 
      child_width = sizes[i] 
    else
      if child_widget:getMaximumWidth() ~= -1 and child_widget:getMaximumWidth() < child_width then
        child_width = child_widget:getMaximumWidth()
      end
    end

    table.insert(child_pos_and_sizes, {
      widget = child_widget,
      x = x,
      y = y,
      width = child_width,
      height = child_height
    })

    if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
      x = x + sizes[i]
    elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
      y = y + sizes[i]
    end
  end

  self:updateChilds(child_pos_and_sizes)
end

--


UI.VBoxLayout = UI.BoxLayout:new{layout_direction = UI.BoxLayoutDirection.Vertical}

UI.HBoxLayout = UI.BoxLayout:new{layout_direction = UI.BoxLayoutDirection.Horizontal}

--

UI.LayoutSizeHint = {
  Fixed = 1,
  Auto = 2
}

UI.Widget = UI.DrawSurface:new()

function UI.Widget:new(o)
  o = UI.DrawSurface:new(o)
  setmetatable(o, self)
  self.__index = self
  o.layout = nil
  o.x = 0
  o.y = 0
  o.height = 0
  o.width = 0
  o.fixed_height_override = nil
  o.fixed_width_override = nil
  return o
end

function UI.Widget:setupUI()
end

function UI.Widget:setFixedHeightOverride(height_override)
  self.fixed_height_override = height_override
end

function UI.Widget:setFixedWidthOverride(width_override)
  self.fixed_width_override = width_override
end

function UI.Widget:getLayoutHeightHint()
  if self.fixed_height_override then
    return UI.LayoutSizeHint.Fixed
  else
    return UI.LayoutSizeHint.Auto
  end
end

function UI.Widget:getMinimumHeight()
  return 1
end

function UI.Widget:getMaximumHeight()
  return -1
end

function UI.Widget:getPreferredHeight()
  if self.fixed_height_override then
    return self.fixed_height_override
  else
    Runtime.pureVirtualCall()
  end
end

function UI.Widget:height()
  return self.height
end

function UI.Widget:getLayoutWidthHint()
  if self.fixed_width_override then
    return UI.LayoutSizeHint.Fixed
  else
    return UI.LayoutSizeHint.Auto
  end
end

function UI.Widget:getMinimumWidth()
  return 1
end

function UI.Widget:getMaximumWidth()
  return -1
end

function UI.Widget:getPreferredWidth()
  if self.fixed_width_override then
    return self.fixed_width_override
  else
    Runtime.pureVirtualCall()
  end
end

function UI.Widget:width()
  return self.width
end

function UI.Widget:clientHeight()
  return self.height
end

function UI.Widget:clientWidth()
  return self.width
end

function UI.Widget:repaint()
  UI.DrawSurface.repaint(self)

  if not self:getLayout() then
    return
  end

  for i, child in ipairs(self:getLayout().child_widgets) do
    child:repaint()
  end
end

function UI.Widget:setPosition(x, y)
  self.x = x
  self.y = y
  self:reposition()
end

function UI.Widget:setSize(width, height)
  self.width = width
  self.height = height
  self:reposition()
end

function UI.Widget:reposition()
  if not self.h then
    Debug.traceback()
  end
  self.h.reposition(self.x, self.y, self.width, self.height)
end

function UI.Widget:getLayout()
  if not self or not self.layout then
    Debug.traceback()
    return nil
  end

  return self.layout
end

function UI.Widget:doLayout()
  self:getLayout():doLayout()
end

function UI.Widget:containsPos(x, y)
  local contains_x = 1 <= x and x <= self.width
  local contains_y = 1 <= y and y <= self.height
  --Debug.print(string.format("  contains_x = %s, contains_y = %s", tostring(contains_x), tostring(contains_y)))
  return contains_x and contains_y
end

function UI.Widget:translateCoordinatesGlobalToLocal(x, y)
  local local_x = x - self.h.absolute_position.x
  local local_y = y - self.h.absolute_position.y
  return local_x, local_y
end

function UI.Widget:translateCoordinatesLocalToGlobal(x, y)
  local global_x = x + self.h.absolute_position.x
  local global_y = y + self.h.absolute_position.y
  return global_x, global_y
end

function UI.Widget:translateCoordinatesLocalToWidget(x, y, widget)
  local global_x, global_y = self:translateCoordinatesLocalToGlobal(x, y)
  --Debug.print(string.format("  L2G %ix%i -> %ix%i", x, y, global_x, global_y))
  local widget_x, widget_y = widget:translateCoordinatesGlobalToLocal(global_x, global_y)
  --Debug.print(string.format("  G2L %ix%i -> %ix%i", global_x, global_y, widget_x, widget_y))
  return widget_x, widget_y
end

function UI.Widget:onMouseButtonPressed(mouse_button, x, y)
  for i,widget in ipairs(self:getLayout().child_widgets) do
    local relative_x, relative_y = self:translateCoordinatesLocalToWidget(x, y, widget)
    local widget_clicked = widget:containsPos(relative_x, relative_y)

    --[[
    Debug.print(string.format("[%i] translate %ix%i rel to %ix%i -> %ix%i rel to %ix%i (%ix%i) = %s",
      i,
      x, y, self.h.absolute_position.x, self.h.absolute_position.y,
      relative_x, relative_y, widget.h.absolute_position.x, widget.h.absolute_position.y,
      widget:clientWidth(), widget:clientHeight(),
      tostring(widget_clicked)))
    ]]

    if widget_clicked then
      widget:onMouseButtonPressed(mouse_button, relative_x, relative_y)
    end
  end
end

--

UI.TabHeader = UI.Widget:new()

function UI.TabHeader:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.is_active = false
  o.text = o.text or "Untitled"
  return o
end

function UI.TabHeader:setActive(is_active)
  self.is_active = is_active
  self:invalidate()
end

function UI.TabHeader:getLayoutWidthHint()
  return UI.LayoutSizeHint.Fixed
end

function UI.TabHeader:getPreferredWidth()
  return string.len(self.text) + 2
end

function UI.TabHeader:doLayout()
end

function UI.TabHeader:repaint()
  local tab_name = string.format(" %s ", self.text)
  self.h.setCursorPos(1, 1)

  if self.is_active then
    self.h.setTextColor(colors.yellow)
    self.h.setBackgroundColor(colors.black) 
  else
    self.h.setTextColor(colors.black)
    self.h.setBackgroundColor(colors.gray)
  end
  self.h.write(tab_name)
end

function UI.TabHeader:onMouseButtonPressed(mouse_button, x, y)
  self.is_active = true
end

--

UI.TabBar = UI.Widget:new()

function UI.TabBar:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.layout = UI.HBoxLayout:new{widget = o}
  o.active_tab_index = 0
  return o
end

function UI.TabBar:getLayoutHeightHint()
  return UI.LayoutSizeHint.Fixed
end

function UI.TabBar:getPreferredHeight()
  return 1
end

function UI.TabBar:addTab(tab_name)
  self:getLayout():addWidget(UI.TabHeader:new{text = tab_name})

  if #self.layout.child_widgets == 1 then
    self.active_tab_index = 1
    self.layout.child_widgets[1].is_active = true
  end
end

function UI.TabBar:repaint()
  self.h.setCursorPos(1, 1)
  self.h.setBackgroundColor(colors.gray)
  self.h.write(string.rep(" ", self.width))

  UI.Widget.repaint(self)
end

function UI.TabBar:setActiveTabIndex(tab_index)
  self.active_tab_index = tab_index

  for i,tab_header in ipairs(self.layout.child_widgets) do
    tab_header.is_active = false
  end
  self.layout.child_widgets[tab_index].is_active = true

  self:invalidate()
end

function UI.TabBar:onMouseButtonPressed(mouse_button, x, y)
  local active_index = nil
  for i,tab_header in ipairs(self.layout.child_widgets) do
    if tab_header.is_active then
      active_index = i
    end
    tab_header.is_active = false
  end

  UI.Widget.onMouseButtonPressed(self, mouse_button, x, y)

  for i,tab_header in ipairs(self.layout.child_widgets) do
    if tab_header.is_active then
      active_index = i
    end
  end

  self.active_tab_index = active_index
  self.layout.child_widgets[active_index].is_active = true
end

--

UI.StackedLayout = UI.Layout:new()

function UI.StackedLayout:doLayout()
  local child_pos_and_sizes = {}
  for i,child_widget in ipairs(self.child_widgets) do
    table.insert(child_pos_and_sizes, {
      widget = child_widget,
      x = 1,
      y = 1,
      width = self.widget:clientWidth(),
      height = self.widget:clientHeight()
    })
  end

  self:updateChilds(child_pos_and_sizes)
end

--

UI.StackedWidget = UI.Widget:new()

function UI.StackedWidget:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.layout = UI.StackedLayout:new{widget = o}
  o.active_widget_index = 1
  return o
end

function UI.StackedWidget:setActiveWidgetIndex(index)
  self.active_widget_index = index
  self:invalidate()
end

function UI.StackedWidget:repaint()
  if self.active_widget_index == nil then
    return
  end

  -- TODO: move repaint to layout!
  for i, widget in ipairs(self:getLayout().child_widgets) do
    if i == self.active_widget_index then
      widget.h.setVisible(true)
      widget:repaint()
    else
      widget.h.setVisible(false)
    end
  end
end

function UI.StackedWidget:onMouseButtonPressed(mouse_button, x, y)
  if self.active_widget_index == nil then
    return
  end
  self:getLayout().child_widgets[self.active_widget_index]:onMouseButtonPressed(mouse_button, x, y) -- TODO: use dispatch rather than direct call?
end

--

UI.TabWidget = UI.Widget:new()

function UI.TabWidget:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.layout = UI.VBoxLayout:new{widget = o}
  o.tab_bar = nil
  o.stacked_widget = nil
  o.tab_list = {}
  return o
end

function UI.TabWidget:setupUI()
  self.tab_bar = UI.TabBar:new()
  self.tab_bar:setupUI()
  self:getLayout():addWidget(self.tab_bar)

  self.stacked_widget = UI.StackedWidget:new()
  self.stacked_widget:setupUI()
  self:getLayout():addWidget(self.stacked_widget)
end

function UI.TabWidget:addTab(key, tab_name, widget)
  table.insert(self.tab_list, key)
  self.tab_bar:addTab(tab_name)
  self.stacked_widget.layout:addWidget(widget)

  self:invalidate()
end

function UI.TabWidget:getIndexOfTab(tab_name)
  for index, v in pairs(self.tab_list) do
    if v == tab_name then
      return index
    end
  end

  return nil
end

function UI.TabWidget:setActiveTab(key)
  local index = self:getIndexOfTab(key)

  self.tab_bar:setActiveTabIndex(index)
  self.stacked_widget:setActiveWidgetIndex(index)

  self:invalidate()
end

function UI.TabWidget:onMouseButtonPressed(mouse_button, x, y)
  UI.Widget.onMouseButtonPressed(self, mouse_button, x, y)

  if self.tab_bar.active_tab_index > 0 then
    self.stacked_widget:setActiveWidgetIndex(self.tab_bar.active_tab_index)
  end
end

--

UI.Screen = UI.DrawSurface:new()

function UI.Screen:new(o)
  o = UI.DrawSurface:new(o)
  setmetatable(o, self)
  self.__index = self
  o.fully_constructed = false
  o.tab_widget = nil
  o.h.clear()
  return o
end

function UI.Screen:setupUI()
  width, height = self.h.getSize()

  self.tab_widget = UI.TabWidget:new()
  self.tab_widget:setupUI()

  self.fully_constructed = true
end

function UI.Screen:doLayout()
  self.tab_widget.h = window.create(self.h, 1, 1, 1, 1)
  self.tab_widget.h.absolute_position = self.h.absolute_position
  self.tab_widget:setPosition(1, 1)
  local width, height = self.h.getSize()
  self.tab_widget:setSize(width, height)
  self.tab_widget:doLayout()
end

function UI.Screen:dispatchEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  local event_handled = self:doDispatch(
    UI.DrawSurface.dispatchEvent,
    {
      monitor_touch = "onMonitorTouched",
      monitor_resize = "onMonitorResized",
    },
    event_name, arg1, arg2, arg3, arg4, arg5)

  if self.fully_constructed then
    self:repaint()
  end

  return event_handled
end

function UI.Screen:addTab(key, tab_name, widget)
  self.tab_widget:addTab(key, tab_name, widget)
end

function UI.Screen:setActiveTab(key)
  self.tab_widget:setActiveTab(key)
end

function UI.Screen:repaint()
  UI.DrawSurface.repaint(self)

  self.tab_widget:repaint()
end

function UI.Screen:onMonitorTouched(side, x, y)
  if self.side ~= side then
    return true
  end
  return self:dispatchEvent("mouse_click", 1, x, y)
end

function UI.Screen:onMouseButtonPressed(mouse_button, x, y)
  self.tab_widget:onMouseButtonPressed(mouse_button, x, y)
end

--

UI.SplitterOrientation = {
  Vertical = 1,
  Horizontal = 2,
}

--

UI.Splitter = UI.Widget:new()

function UI.Splitter:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.is_active = false
  return o
end

function UI.Splitter:getLayoutWidthHint()
  if self.orientation == UI.SplitterOrientation.Vertical then
    return UI.LayoutSizeHint.Fixed
  elseif self.orientation == UI.SplitterOrientation.Horizontal then
    return UI.LayoutSizeHint.Auto
  end
end

function UI.Splitter:getPreferredWidth()
  return 1
end

function UI.Splitter:getLayoutHeightHint()
  if self.orientation == UI.SplitterOrientation.Vertical then
    return UI.LayoutSizeHint.Auto
  elseif self.orientation == UI.SplitterOrientation.Horizontal then
    return UI.LayoutSizeHint.Fixed
  end
end

function UI.Splitter:getPreferredHeight()
  return 1
end

function UI.Splitter:doLayout()
end

function UI.Splitter:repaint()
  local normal_symbol = '?'
  local active_symbol = '?'
  local symbol = '?'
  
  if self.orientation == UI.SplitterOrientation.Vertical then
    normal_symbol = string.char(0x95)
    active_symbol = string.char(0x91)
  elseif self.orientation == UI.SplitterOrientation.Horizontal then
    normal_symbol = string.char(0x83)
    active_symbol = string.char(0x81)
  end

  if self.is_active then
    symbol = active_symbol
  else
    symbol = normal_symbol
  end

  if self.orientation == UI.SplitterOrientation.Vertical then
    for y=1, self:clientHeight() do
      self.h.setCursorPos(1, y)
      self.h.write(symbol)
    end
  elseif self.orientation == UI.SplitterOrientation.Horizontal then
    for x=1, self:clientWidth() do
      self.h.setCursorPos(x, 1)
      self.h.write(symbol)
    end
  end
end

function UI.Splitter:onMouseButtonPressed(mouse_button, x, y)
  self.is_active = not self.is_active
end

--

UI.HSplitter = UI.Splitter:new{orientation=UI.SplitterOrientation.Horizontal}

UI.VSplitter = UI.Splitter:new{orientation=UI.SplitterOrientation.Vertical}

--

UI.SplitLayout = UI.BoxLayout:new()

function UI.SplitLayout:new(o)
  o = UI.BoxLayout:new(o)
  setmetatable(o, self)
  self.__index = self
  o.layouted = false
  return o
end

function UI.SplitLayout:doLayout()
  if not self.layouted then
    self.layouted = true
    UI.BoxLayout.doLayout(self)
    return
  end

  for _, widget in ipairs(self.child_widgets) do
    widget.h.absolute_position = {
      x = self.widget.h.absolute_position.x + widget.x - 1,
      y = self.widget.h.absolute_position.y + widget.y - 1}
    widget:setPosition(widget.x, widget.y)
    if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
      widget:setSize(widget.width, self.widget:clientHeight())
    else
      widget:setSize(self.widget:clientWidth(), widget.height)
    end
    widget:doLayout()
  end
end

function UI.SplitLayout:setSplitterPosition(splitter, delta)
  local splitter_widget_index = -1
  for index, widget in ipairs(self.child_widgets) do
    if widget == splitter then
      splitter_widget_index = index
      break
    end
  end

  return self:setSplitterPositionImpl(splitter, splitter_widget_index, delta)
end

function UI.SplitLayout:setSplitterPositionByIndex(splitter_index, delta)
  local splitter_widget_index = splitter_index * 2
  return self:setSplitterPositionImpl(
    self.child_widgets[splitter_widget_index], 
    splitter_widget_index, 
    delta)
end

function UI.SplitLayout:setSplitterPositionAbsoluteByIndex(splitter_index, position)
  local available_size = 0
  if self.layout_direction == UI.BoxLayoutDirection.Vertical then
    available_size = self.widget:clientHeight()
  else
    available_size = self.widget:clientWidth()
  end

  local target_splitter_position = math.floor(position / 100 * available_size)

  local splitter_widget_index = splitter_index * 2
  local current_splitter_positon = 0
  for widget_index, widget in ipairs(self.child_widgets) do
    if widget_index == splitter_widget_index then
      break
    end

    if self.layout_direction == UI.BoxLayoutDirection.Vertical then
      child_widget_size = widget:clientHeight()
    else
      child_widget_size = widget:clientWidth()
    end
    current_splitter_positon = current_splitter_positon + child_widget_size
  end

  return self:setSplitterPositionImpl(
    self.child_widgets[splitter_widget_index], 
    splitter_widget_index, 
    target_splitter_position - current_splitter_positon)
end

function UI.SplitLayout:setSplitterPositionImpl(splitter, splitter_widget_index, delta)
  local previous_widget = self.child_widgets[splitter_widget_index - 1]
  local next_widget = self.child_widgets[splitter_widget_index + 1]

  local splitter_relative_x, splitter_relative_y = splitter:translateCoordinatesLocalToWidget(1, 1, self.widget)
  local previous_relative_x, previous_relative_y = previous_widget:translateCoordinatesLocalToWidget(1, 1, self.widget)
  local next_relative_x, next_relative_y = next_widget:translateCoordinatesLocalToWidget(1, 1, self.widget)

  if self.layout_direction == UI.BoxLayoutDirection.Horizontal then
    if delta < 0 then
      if -delta > splitter_relative_x - previous_relative_x - 1 then
        delta = -(splitter_relative_x - previous_relative_x - 1)
      end
    elseif delta > 0 then
      if delta > next_widget.width - 1 then
        delta = next_widget.width - 1
      end
    end

    previous_widget.width = previous_widget.width + delta
    splitter.x = splitter.x + delta   
    next_widget.width = next_widget.width - delta
    next_widget.x = next_widget.x + delta
  elseif self.layout_direction == UI.BoxLayoutDirection.Vertical then
    if delta < 0 then
      if -delta > splitter_relative_y - previous_relative_y - 1 then
        delta = -(splitter_relative_y - previous_relative_y - 1)
      end
    elseif delta > 0 then
      if delta > next_widget.height - 1 then
        delta = next_widget.height - 1
      end
    end

    previous_widget.height = previous_widget.height + delta
    splitter.y = splitter.y + delta   
    next_widget.height = next_widget.height - delta
    next_widget.y = next_widget.y + delta
  end

  previous_widget.h.absolute_position = {
    x = self.widget.h.absolute_position.x + previous_widget.x - 1,
    y = self.widget.h.absolute_position.y + previous_widget.y - 1}
  previous_widget:setPosition(previous_widget.x, previous_widget.y)
  previous_widget:setSize(previous_widget.width, previous_widget.height)
  previous_widget:doLayout()
 
  splitter.h.absolute_position = {
    x = self.widget.h.absolute_position.x + splitter.x - 1,
    y = self.widget.h.absolute_position.y + splitter.y - 1}
  splitter:setPosition(splitter.x, splitter.y)
  splitter:setSize(splitter.width, splitter.height)
  splitter:doLayout()

  next_widget.h.absolute_position = {
    x = self.widget.h.absolute_position.x + next_widget.x - 1,
    y = self.widget.h.absolute_position.y + next_widget.y - 1}
  next_widget:setPosition(next_widget.x, next_widget.y)
  next_widget:setSize(next_widget.width, next_widget.height)
  next_widget:doLayout()
end

--

UI.HSplitLayout = UI.SplitLayout:new{layout_direction = UI.BoxLayoutDirection.Horizontal}

UI.VSplitLayout = UI.SplitLayout:new{layout_direction = UI.BoxLayoutDirection.Vertical}

--

UI.SplitView = UI.Widget:new()

function UI.SplitView:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

function UI.SplitView:splitVertical(num_splits)
  self.layout = UI.HSplitLayout:new{widget = self}
  
  local splits = {}
  for i=1,num_splits do
    if i > 1 then
      self.layout:addWidget(UI.VSplitter:new())
    end
    local split = UI.SplitView:new()
    table.insert(splits, split)
    self.layout:addWidget(split)
  end

  return unpack(splits)
end

function UI.SplitView:splitHorizontal(num_splits)
  self.layout = UI.VSplitLayout:new{widget = self}
  
  local splits = {}
  for i=1,num_splits do
    if i > 1 then
      self.layout:addWidget(UI.HSplitter:new())
    end
    local split = UI.SplitView:new()
    table.insert(splits, split)
    self.layout:addWidget(split)
  end
  
  return unpack(splits)
end

function UI.SplitView:setWidget(widget)
  self.layout = UI.StackedLayout:new{widget = self}

  self.layout:addWidget(widget)
end

function UI.SplitView:onMouseButtonPressed(mouse_button, x, y)
  for _, widget in ipairs(self.layout.child_widgets) do
    if widget.is_active then
      local relative_x, relative_y = widget:translateCoordinatesGlobalToLocal(x, y)
      if self.layout.layout_direction == UI.BoxLayoutDirection.Horizontal then
        self.layout:setSplitterPosition(widget, relative_x)
      elseif self.layout.layout_direction == UI.BoxLayoutDirection.Vertical then
        self.layout:setSplitterPosition(widget, relative_y)
      end
      widget.is_active = false
      return true
    end
  end

  UI.Widget.onMouseButtonPressed(self, mouse_button, x, y)
end

--

UI.Spacer = UI.Widget:new()

function UI.Spacer:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  self.auto_height = true
  self.preferred_height = nil
  self.auto_width = true
  self.preferred_width = nil
  self.layout = UI.NullLayout:new()
  return o
end

function UI.Spacer:getLayoutHeightHint()
  if self.auto_height then 
    return UI.LayoutSizeHint.Auto 
  else 
    return UI.LayoutSizeHint.Fixed 
  end
end

function UI.Spacer:getMinimumHeight()
  return 1
end

function UI.Spacer:getMaximumHeight()
  return self.preferred_height
end

function UI.Spacer:getPreferredHeight()
  return self.preferred_height
end

function UI.Spacer:getLayoutWidthHint()
  if self.auto_width then 
    return UI.LayoutSizeHint.Auto 
  else 
    return UI.LayoutSizeHint.Fixed 
  end
end

function UI.Spacer:getMinimumWidth()
  return 1
end

function UI.Spacer:getMaximumWidth()
  return self.preferred_width
end

function UI.Spacer:getPreferredWidth()
  return self.preferred_width
end

function UI.Spacer:onMouseButtonPressed(...)
end


--

UI.ScrollDirection = {
  Lower = 1,
  Higher = 2,
}

UI.ScrollBar = UI.Widget:new()

function UI.ScrollBar:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.handle_size = o.handle_size or 1
  o.handle_position = o.handle_position or 0.5
  o.direction = o.direction or UI.BoxLayoutDirection.Vertical
  o.scroll_callback = function(v) end
  o.increment_callback = function(direction)
    if self.direction == UI.BoxLayoutDirection.Vertical then
      handle_range = self:clientHeight() - 2
    else
      handle_range = self:clientWidth() - 2
    end

    if direction == UI.ScrollDirection.Lower then
      delta = -1
    elseif direction == UI.ScrollDirection.Higher then
      delta = 1
    end

    return delta / handle_range
  end
  return o
end

function UI.ScrollBar:doLayout()
end

function UI.ScrollBar:getLayoutHeightHint()
  if self.direction == UI.BoxLayoutDirection.Vertical then
    return UI.LayoutSizeHint.Auto
  else
    return UI.LayoutSizeHint.Fixed
  end
end

function UI.ScrollBar:getPreferredHeight()
  if self.direction == UI.BoxLayoutDirection.Vertical then
    return -1
  else
    return 1
  end
end

function UI.ScrollBar:getMaximumHeight()
  if self.direction == UI.BoxLayoutDirection.Vertical then
    return -1
  else
    return 1
  end
end

function UI.ScrollBar:getLayoutWidthHint()
  if self.direction == UI.BoxLayoutDirection.Horizontal then
    return UI.LayoutSizeHint.Auto
  else
    return UI.LayoutSizeHint.Fixed
  end
end

function UI.ScrollBar:getPreferredWidth()
  if self.direction == UI.BoxLayoutDirection.Horizontal then
    return -1
  else
    return 1
  end
end

function UI.ScrollBar:getMaximumWidth()
  if self.direction == UI.BoxLayoutDirection.Horizontal then
    return -1
  else
    return 1
  end
end

function UI.ScrollBar:repaint()
  if self.direction == UI.BoxLayoutDirection.Vertical then
    local handle_range = self:clientHeight() - 3
    local handle_display_pos = math.floor(self.handle_position * handle_range) + 2

    self.h.setCursorPos(1, 1)
    self.h.blit(string.char(0x1e), "0", "7")

    for y=2,self:clientHeight()-1 do
      self.h.setCursorPos(1, y)
      if y == handle_display_pos then
        --self.h.blit(string.char(0x7f), "b", "7")
        self.h.blit(string.char(0x8f), "7", "8")
      else
        self.h.blit(" ", "7", "8")
      end
    end

    self.h.setCursorPos(1, self:clientHeight())
    self.h.blit(string.char(0x1f), "0", "7")
  else
    self.h.setCursorPos(1, 1)
    
    local handle_range = self:clientWidth() - 2
    local pre_handle_size = math.floor(self.handle_position * handle_range)
    local post_handle_size = handle_range - pre_handle_size - 1
    if post_handle_size < 0 then
      pre_handle_size = pre_handle_size + post_handle_size
      post_handle_size = 0
    end

    self.h.blit(
      string.char(0x11)
      .. string.rep(" ", pre_handle_size)
      .. string.char(0x8f)
      .. string.rep(" ", post_handle_size)
      .. string.char(0x10),
      "0" .. string.rep("7", handle_range) .. "0",
      "7" .. string.rep("8", handle_range) .. "7")
  end
end

function UI.ScrollBar:onMouseButtonPressed(mouse_button, x, y)
  local handle_range
  local delta = 0

  if self.direction == UI.BoxLayoutDirection.Vertical then
    handle_range = self:clientHeight() - 2
    if y == 1 then
      delta = self.increment_callback(UI.ScrollDirection.Lower)
    elseif y == self:clientHeight() then
      delta = self.increment_callback(UI.ScrollDirection.Higher)
    end
  else
    handle_range = self:clientWidth() - 2
    if x == 1 then
      delta = self.increment_callback(UI.ScrollDirection.Lower)
    elseif x == self:clientWidth() then
      delta = self.increment_callback(UI.ScrollDirection.Higher)
    end
  end

  local previous_pos = self.handle_position
  self.handle_position = self.handle_position + delta
  if self.handle_position < 0 then
    self.handle_position = 0
  end
  if self.handle_position > 1 then
    self.handle_position = 1
  end

  if previous_pos ~= self.handle_position then
    self:invalidate()
    self.scroll_callback(self.handle_position)
  end
end

function UI.ScrollBar:setScrollCallback(fn)
  self.scroll_callback = fn
end

function UI.ScrollBar:setIncrementCallback(fn)
  self.increment_callback = fn
end

--

UI.HScrollBar = UI.ScrollBar:new{direction = UI.BoxLayoutDirection.Horizontal}

UI.VScrollBar = UI.ScrollBar:new{direction = UI.BoxLayoutDirection.Vertical}

--

UI.ScrollLayout = UI.Layout:new()

function UI.ScrollLayout:new(o)
  o = UI.Layout:new(o)
  setmetatable(o, self)
  self.__index = self
  self.scroll_position = {
    x = 0,
    y = 0
  }
  return o
end

function UI.ScrollLayout:doLayout()
  local child_widget = self.child_widgets[1];

  self:updateChilds({{
    widget = child_widget,
    x = -self.scroll_position.x,
    y = -self.scroll_position.y,
    width = math.max(child_widget:getPreferredWidth(), self.widget:clientWidth()),
    height = math.max(child_widget:getPreferredHeight(), self.widget:clientHeight())
  }})
end

function UI.ScrollLayout:setScrollPositionX(x)
  self.scroll_position.x = x
end

function UI.ScrollLayout:setScrollPositionY(y)
  self.scroll_position.y = y
end

--

UI.ScrollBox = UI.Widget:new()

function UI.ScrollBox:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.central_widget = nil
  return o
end

function UI.ScrollBox:setupUI()
  self.layout = UI.VBoxLayout:new{widget = self}

  local w = UI.Widget:new{}
  w.layout = UI.HBoxLayout:new{widget = w}

  -- content widget
  self.scroll_widget = UI.Widget:new()
  self.scroll_widget.layout = UI.ScrollLayout:new{widget = self.scroll_widget}  
  self.scroll_widget:getLayout():addWidget(self.central_widget)
  w:getLayout():addWidget(self.scroll_widget)

  -- vertical scrollbar
  local vscrollbar = UI.VScrollBar:new()
  vscrollbar.handle_position = 0
  w:getLayout():addWidget(vscrollbar)

  self:getLayout():addWidget(w)

  local horizontal_scrollbar_container = UI.Widget:new()
  horizontal_scrollbar_container.layout = UI.HBoxLayout:new{widget = horizontal_scrollbar_container}
  
  -- horizontal scrollbar
  local hscrollbar = UI.HScrollBar:new()
  hscrollbar.handle_position = 0
  horizontal_scrollbar_container:getLayout():addWidget(hscrollbar)

  -- 1x1 spacer
  local edge_spacer = UI.Spacer:new()
  edge_spacer.auto_width = false
  edge_spacer.preferred_width = 1
  edge_spacer.auto_height = false
  edge_spacer.preferred_height = 1
  horizontal_scrollbar_container:getLayout():addWidget(edge_spacer)

  self:getLayout():addWidget(horizontal_scrollbar_container)

  -- Scroll Callbacks
  vscrollbar:setScrollCallback(function(v)
    local scroll_height = self.central_widget:clientHeight() - self.scroll_widget:clientHeight()
    if scroll_height < 0 then
      scroll_height = 0
    end
    self.scroll_widget:getLayout():setScrollPositionY(
      math.floor(v * scroll_height))
    self.scroll_widget:getLayout():doLayout()
    self.scroll_widget:invalidate()
  end)

  hscrollbar:setScrollCallback(function(v)
    local scroll_width = self.central_widget:clientWidth() - self.scroll_widget:clientWidth()
    if scroll_width < 0 then
      scroll_width = 0
    end
    self.scroll_widget:getLayout():setScrollPositionX(
      math.floor(v * scroll_width))
    self.scroll_widget:getLayout():doLayout()
    self.scroll_widget:invalidate()
  end)

  -- Increment Callbacks
  vscrollbar:setIncrementCallback(function(direction)
    if direction == UI.ScrollDirection.Lower then
      return -1 / self.central_widget:clientHeight()
    else
      return 1 / self.central_widget:clientHeight()
    end
  end)

  hscrollbar:setIncrementCallback(function(direction)
    if direction == UI.ScrollDirection.Lower then
      return -1 / self.central_widget:clientWidth()
    else
      return 1 / self.central_widget:clientWidth()
    end
  end)
end

function UI.ScrollBox:setWidget(widget)
  self.central_widget = widget
end

--

UI.Label = UI.Widget:new()

function UI.Label:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.text = o.text or ""
  return o
end

function UI.Label:setText(text)
  self.text = text
end

function UI.Label:doLayout()
end

function UI.Label:getPreferredWidth()
  return #self.text
end

function UI.Label:repaint()
  self.h.setCursorPos(1, 1)
  self.h.blit(self.text, 
    string.rep("8", #self.text), 
    string.rep("f", #self.text))
end

--

UI.StatusBar = UI.Widget:new()

function UI.StatusBar:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  o.items = {}
  return o
end

function UI.StatusBar:addItem(item)
  table.insert(self.items, item)
end

function UI.StatusBar:setupUI()
  self.layout = UI.HBoxLayout:new{widget=self}

  for i, item in pairs(self.items) do
    if type(item) == "string" then
      self.layout:addWidget(UI.Label:new{text=item})
    else
      self.layout:addWidget(item)
    end
  end
end

function UI.StatusBar:getLayoutHeightHint()
  return UI.LayoutSizeHint.Fixed
end

function UI.StatusBar:getPreferredHeight()
  return 1
end

function UI.StatusBar:getMaximumHeight()
  return 1
end

--

UI.UserInterface = {}

function UI.UserInterface:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function UI.UserInterface:wrapTerm()
  term_handle = term.native()
  term_handle.absolute_position = {
    x = 1,
    y = 1
  }
  local screen = UI.Screen:new{h = term_handle}

  self.event_loop:registerHandler(screen, {
    "repaint",
    "char",
    "key",
    "paste",
    "mouse_click",
    "mouse_drag",
    "mouse_scroll"
  })
  return screen
end

function UI.UserInterface:wrapMonitor(peripheral_name)
  local screen_handle = peripheral.wrap(peripheral_name)
  screen_handle.absolute_position = {
    x = 1,
    y = 1
  }
  screen_handle.setTextScale(0.5)
  local screen = UI.Screen:new{
    h = screen_handle, 
    side = peripheral_name}

  self.event_loop:registerHandler(screen, {
    "repaint",
    "monitor_touch",
    "monitor_resize"
  })
  return screen
end

function UI.UserInterface:createPerspective(perspective_name)
  return UI.SplitView:new()
end