require "ui"
require "ui_loader"
require "event"
require "logging"
require "main_loop"
require "dispatch"
require "net"

--

SystemManagement = {}

SystemManagement.EventLoop = Event.EventLoop:new()
SystemManagement.UI = UI.UserInterface:new{event_loop = SystemManagement.EventLoop}
SystemManagement.UILoader = UI.Loader.Loader:new()
SystemManagement.Logger = Logging.Logger:new{event_loop = SystemManagement.EventLoop}
SystemManagement.MainLoop = MainLoop.MainLoop:new{logger = SystemManagement.Logger}
SystemManagement.WorkContext = Dispatch.DispatchLoop:new()
SystemManagement.Networking = Networking.ModemManager:new()

SystemManagement.MainLoop:registerMainFn(function()
  SystemManagement.EventLoop:run()
end)

SystemManagement.MainLoop:registerMainFn(function()
  SystemManagement.WorkContext:run()
end)
