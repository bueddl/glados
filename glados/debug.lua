Debug = {raw_redirect = false, logger = nil}

function Debug.print(...)
  if raw_redirect then
    t = term.current()
    term.redirect(peripheral.wrap("monitor_2"))
    print(...)
    term.redirect(t)
  else
    local args = {...}
    for i,arg in ipairs(args) do
      args[i] = tostring(args[i])
    end
    local message = table.concat(args, " ")
    Debug.logger:debug("[dbg]", message)
  end
end

function Debug.traceback()
  for line in debug.traceback():gmatch("([^\n]*)\n?") do
    Debug.print(line)
  end
end

function Debug.dump(o)
  if o == nil then
    Debug.print("nil")
    return
  end

  local out = "{"
  local isFirst = true
  for k,v in pairs(o) do
    if not isFirst then
      out = out .. ", "
    end
    isFirst = false
    out = out .. string.format("%s=%s", k, v)
  end
  out = out .. "}"
  Debug.print(out)
end
