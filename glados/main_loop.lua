require "logging"

MainLoop = {}

MainLoop.MainLoop = {}

function MainLoop.MainLoop:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.registered_coros = {}
  return o
end

function MainLoop.MainLoop:registerMainFn(fn)
  table.insert(self.registered_coros, fn)
end

function MainLoop.MainLoop:run()
  self.logger:info("MainLoop", "Starting main loop")
  local stopped_function = parallel.waitForAny(unpack(self.registered_coros))
  self.logger:error("MainLoop", "main fn exited, main loop stopped. no = %d, fn = %s",
    stopped_function,
    self.registered_coros[stopped_function])
end
