require "ui"

--

DummyWidget = {}

DummyWidget.DummyWidget = UI.Widget:new{text = "Example", color = colors.blue, text_color = colors.black}

function DummyWidget.DummyWidget:new(o)
  o = UI.Widget:new(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

function DummyWidget.DummyWidget:doLayout()
end

function DummyWidget.DummyWidget:repaint()
  t = term.current()
  term.redirect(self.h)

  local text = string.format("%dx%d at %dx%d (%dx%d)",
    self:clientWidth(),
    self:clientHeight(),
    self.x, self.y,
    self.h.absolute_position.x, self.h.absolute_position.y)

  paintutils.drawFilledBox(1, 1, self.width, self.height, self.color)
  self.h.setCursorPos((self.width - string.len(text)) / 2, self.height / 2)
  self.h.setBackgroundColor(self.color)
  self.h.setTextColor(self.text_color)
  self.h.write(text)

  if self._click_pos ~= nil then
    self.h.setCursorPos(self._click_pos.x, self._click_pos.y)
    self.h.setBackgroundColor(self.text_color)
    self.h.write(" ")
  end

  term.redirect(t)
end

function DummyWidget.DummyWidget:onMouseButtonPressed(mouse_button, x, y)
  self._click_pos = {x=x, y=y}
  self.logger:info("DummyWidget.DummyWidget", "My Text is '%s'", self.text)
  return true
end

--

DummyWidget.LargeWidget = UI.ScrollBox:new()

function DummyWidget.LargeWidget:new(o)
  o = UI.ScrollBox:new(o)
  setmetatable(o, self)
  self.__index = self
  self.loglines = {}
  return o
end

function DummyWidget.LargeWidget:doLayout()
end

function DummyWidget.LargeWidget:getPreferredWidth()
  return 402
end

function DummyWidget.LargeWidget:getPreferredHeight()
  return 300
end

function DummyWidget.LargeWidget:repaint()
  t = term.current()
  term.redirect(self.h)

  local pattern = "00112233445566778899aabbccddeeff"
  local pattern_repetitions = math.floor(self:getPreferredWidth() / #pattern)
  local display_pattern = string.rep(pattern, pattern_repetitions)

  local current_length = pattern_repetitions * #pattern
  for i = current_length + 1, self:getPreferredWidth() do
    display_pattern = display_pattern .. pattern:sub(i % #pattern + 1, i % #pattern + 1)
  end

  for y = 1, self:getPreferredHeight() do
    self.h.setCursorPos(1, y)

    local this_pattern = display_pattern:sub((y % #pattern) + 1, -1) .. display_pattern:sub(#pattern - (y % #pattern), #pattern - 1)
    
    self.h.blit(
      string.rep(" ", self:getPreferredWidth()),
       this_pattern,
       this_pattern)
  end

  term.redirect(t)
end

function DummyWidget.LargeWidget:onMouseButtonPressed()
end

DummyWidget.ExampleScrollWidget = UI.ScrollBox:new()

function DummyWidget.ExampleScrollWidget:new(o)
  o = UI.ScrollBox:new(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

function DummyWidget.ExampleScrollWidget:setupUI()
  self:setWidget(DummyWidget.LargeWidget:new())
  UI.ScrollBox.setupUI(self)
end
