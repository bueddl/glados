require "system"
require "net"
require "debug"
require "server"

Debug.logger = SystemManagement.Logger

settings.load(".settings")
SystemManagement.UILoader:load(settings)

local server = Server.Server:new()
server:start(1234)

SystemManagement.EventLoop:queueEvent("repaint")
SystemManagement.MainLoop:run()
