require "debug"
require "runtime"

--

Event = {}

--

Event.EventHandler = {}

function Event.EventHandler:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Event.EventHandler:dispatchEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  return false
end

function Event.EventHandler:doDispatch(previous_handler, handler_map, event_name, arg1, arg2, arg3, arg4, arg5)

  if previous_handler(self, event_name, arg1, arg2, arg3, arg4, arg5) then
    return true
  end

  local dispatched_event_name = handler_map[event_name]
  if dispatched_event_name == nil then
    return false
  end

  local event_handler = self[dispatched_event_name]
  if event_handler == nil then
    return false
  end
  
  return event_handler(self, arg1, arg2, arg3, arg4, arg5)
end

--

Event.EventLoop = {}

function Event.EventLoop:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.registered_handlers = {}
  o.stop = false
  return o
end

function Event.EventLoop:run()
  while not self.stop do
    self:doRunOne()
  end
end

function Event.EventLoop:doRunOne()
  require "system"

  event_name, arg1, arg2, arg3, arg4, arg5 = os.pullEvent()

  if event_name ~= "new_log_entry" then
    SystemManagement.Logger:debug("Event.EventLoop", "New event: %s(%s, %s, %s, %s, %s)", event_name, arg1, arg2, arg3, arg4, arg5)
  end

  if self.registered_handlers[event_name] == nil then
    return
  end

  for _, handler in pairs(self.registered_handlers[event_name]) do
    handler:dispatchEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  end
end

function Event.EventLoop:registerHandler(object, event_list)
  for _, event_name in pairs(event_list) do
    if self.registered_handlers[event_name] == nil then
      self.registered_handlers[event_name] = {}
    end

    table.insert(self.registered_handlers[event_name], object)
  end
end

function Event.EventLoop:queueEvent(event_name, arg1, arg2, arg3, arg4, arg5)
  os.queueEvent(event_name, arg1, arg2, arg3, arg4, arg5)
end
