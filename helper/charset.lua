term.clear()
y = 1
x = 3
for i=0,15 do
  term.setCursorPos(x, y)
  x = x  + 2
  write(string.format("%x ", i))
end
y = y + 1
x = 1

for i=0,255 do
  if i % 16 == 1 then
    y = y + 1
    x = 1
    term.setCursorPos(x, y)
    write(string.format("%x ", math.floor(i/16)))
    x = 3
  end
  term.setCursorPos(x, y)
  x = x  + 2
  write(string.format("%s ", string.char(i)))
end
print("")